<?php
/***************************************************************************
 *   ZeeAuctions Ebay Clone
 *   site					: http://www.zeeauctions.com/
 ***************************************************************************/

/***************************************************************************
 *   This program is distributed under the GNU General Public License v2.
 ***************************************************************************/

define('InAdmin', 1);
$current_page = 'interface';
include '../includes/common.inc.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

if (isset($_POST['action']) && $_POST['action'] == 'update')
{
	if (is_dir($main_path . 'cache'))
	{
		$dir = opendir($main_path . 'cache');
		while (($myfile = readdir($dir)) !== false)
		{
			if ($myfile != '.' && $myfile != '..' && $myfile != 'index.php')
			{
				unlink($main_path . 'cache/' . $myfile);
			}
		}
		closedir($dir);
	}
	$ERR = $MSG['30_0033'];
}

$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'SITEURL' => $system->SETTINGS['siteurl']
		));

$template->set_filenames(array(
		'body' => 'clearcache.tpl'
		));
$template->display('body');
?>
