<?php
/***************************************************************************
 *   ZeeAuctions Ebay Clone
 *   site					: http://www.zeeauctions.com/
 ***************************************************************************/

/***************************************************************************
 *   This program is distributed under the GNU General Public License v2.
 ***************************************************************************/

define('InAdmin', 1);
$current_page = 'settings';
include '../includes/common.inc.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';
include $main_path . 'ckeditor/ckeditor.php';
include $include_path . 'HTMLPurifier/HTMLPurifier.auto.php';

unset($ERR);

if (isset($_POST['action']) && $_POST['action'] == 'update')
{
	$conf = HTMLPurifier_Config::createDefault();
	$conf->set('Core', 'Encoding', $CHARSET); // replace with your encoding
	$conf->set('HTML', 'Doctype', 'HTML 4.01 Transitional'); // replace with your doctype
	$purifier = new HTMLPurifier($conf);

	// Update database
	$query = "UPDATE " . $DBPrefix . "settings SET
			  errortext = '" . $purifier->purify($_POST['errortext']) . "'";
	$system->check_mysql(mysql_query($query), $query, __LINE__, __FILE__);
	$system->SETTINGS['errortext'] = $_POST['errortext'];
	$ERR = $MSG['413'];
}

$CKEditor = new CKEditor();
$CKEditor->basePath = $main_path . 'ckeditor/';
$CKEditor->returnOutput = true;
$CKEditor->config['width'] = 550;
$CKEditor->config['height'] = 400;

loadblock($MSG['411'], $MSG['410'], $CKEditor->editor('errortext', stripslashes($system->SETTINGS['errortext'])));

$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'SITEURL' => $system->SETTINGS['siteurl'],
		'TYPENAME' => $MSG['5142'],
		'PAGENAME' => $MSG['409']
		));

$template->set_filenames(array(
		'body' => 'adminpages.tpl'
		));
$template->display('body');
?>
