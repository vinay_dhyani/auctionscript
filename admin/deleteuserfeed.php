<?php
/***************************************************************************
 *   ZeeAuctions Ebay Clone
 *   site					: http://www.zeeauctions.com/
 ***************************************************************************/

/***************************************************************************
 *   This program is distributed under the GNU General Public License v2.
 ***************************************************************************/


define('InAdmin', 1);
include '../includes/common.inc.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

$id = intval($_REQUEST['id']);
$user_id = intval($_REQUEST['user']);

if (isset($_POST['action']) && $_POST['action'] == $MSG['030'])
{
	$query = "DELETE FROM " . $DBPrefix . "feedbacks WHERE id = " . $id;
	$system->check_mysql(mysql_query($query), $query, __LINE__, __FILE__);
	$query = "SELECT SUM(rate) as FSUM, count(feedback) as FNUM FROM " . $DBPrefix . "feedbacks WHERE rated_user_id = " . $user_id;
	$res = mysql_query($query);
	$system->check_mysql($res, $query, __LINE__, __FILE__);
	$fb_data = mysql_fetch_assoc($res);
	$query = "UPDATE " . $DBPrefix . "users SET rate_sum = " . $fb_data['SUM'] . ", rate_num = " . $fb_data['NUM'] . " WHERE id = " . $user_id;
	$system->check_mysql(mysql_query($query), $query, __LINE__, __FILE__);
	header('location: userfeedback.php?id=' . $user_id);
	exit;
}
elseif (isset($_POST['action']) && $_POST['action'] == $MSG['029'])
{
	header('location: userfeedback.php?id=' . $user_id);
	exit;
}

$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'ID' => $id,
		'USERID' => $user_id,
		'MESSAGE' => sprintf($MSG['848'], $id),
		'TYPE' => 2
		));

$template->set_filenames(array(
		'body' => 'confirm.tpl'
		));
$template->display('body');

?>