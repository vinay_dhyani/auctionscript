<?php
/***************************************************************************
 *   ZeeAuctions Ebay Clone
 *   site					: http://www.zeeauctions.com/
 ***************************************************************************/

/***************************************************************************
 *   This program is distributed under the GNU General Public License v2.
 ***************************************************************************/

define('InAdmin', 1);
include "../includes/common.inc.php";
include $include_path . 'functions_admin.php';

$template->assign_vars(array(
		'SITEURL' => $system->SETTINGS['siteurl'],
		'B_ADMINLOGIN' => (!checklogin())
		));

$template->set_filenames(array(
		'body' => 'adminbar.tpl'
		));
$template->display('body');
?>