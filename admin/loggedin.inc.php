<?php
/***************************************************************************
 *   ZeeAuctions Ebay Clone
 *   site					: http://www.zeeauctions.com/
 ***************************************************************************/

/***************************************************************************
 *   This program is distributed under the GNU General Public License v2.
 ***************************************************************************/

if (checklogin())
{
	header("location: login.php");
	exit;
}
else
{
	$mth = 'MON_0' . gmdate('m', $_SESSION['WEBID_ADMIN_TIME']);
	$return = gmdate('d', $_SESSION['WEBID_ADMIN_TIME']) . ' ' . $MSG[$mth] . ', ' . gmdate('Y - H:i', $_SESSION['WEBID_ADMIN_TIME']);
	$template->assign_vars(array(
			'DOCDIR' => $DOCDIR,
			'THEME' => $system->SETTINGS['theme'],
			'SITEURL' => $system->SETTINGS['siteurl'],
			'CHARSET' => $CHARSET,
			'EXTRAJS' => (isset($extraJs)) ? $extraJs : '',
			'ADMIN_USER' => $_SESSION['WEBID_ADMIN_USER'],
			'ADMIN_ID' => $_SESSION['WEBID_ADMIN_IN'],
			'CURRENT_PAGE' => $current_page,
			'LAST_LOGIN' => $return
			));
	foreach ($LANGUAGES as $lang => $value)
	{
		$template->assign_block_vars('langs', array(
				'LANG' => $value,
				'B_DEFAULT' => ($lang == $system->SETTINGS['defaultlanguage'])
				));
	}
}
?>