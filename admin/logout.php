<?php
/***************************************************************************
 *   ZeeAuctions Ebay Clone
 *   site					: http://www.zeeauctions.com/
 ***************************************************************************/

/***************************************************************************
 *   This program is distributed under the GNU General Public License v2.
 ***************************************************************************/

define('InAdmin', 1);
include '../includes/common.inc.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

unset($_SESSION['WEBID_ADMIN_NUMBER'], $_SESSION['WEBID_ADMIN_PASS'], $_SESSION['WEBID_ADMIN_IN'], $_SESSION['WEBID_ADMIN_USER']);
?>
<script type="text/javascript">
parent.location.href = 'index.php';
</script>