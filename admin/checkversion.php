<?php
/***************************************************************************
 *   ZeeAuctions Ebay Clone
 *   site					: http://www.zeeauctions.com/
 ***************************************************************************/

/***************************************************************************
 *   This program is distributed under the GNU General Public License v2.
 ***************************************************************************/

define('InAdmin', 1);
$current_page = 'tools';
include '../includes/common.inc.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

if (!($realversion = load_file_from_url('http://www.zeeauctions.com/version.txt')))
{
	$ERR = $ERR_25_0002;
	$realversion = 'Unknown';
}

if ($realversion != $system->SETTINGS['version'])
{
	$myversion = '<span style="color:#ff0000;">' . $system->SETTINGS['version'] . '</span>';
	$text = $MSG['30_0211'];
}
else
{
	$myversion = '<span style="color:#00ae00;">' . $system->SETTINGS['version'] . '</span>';
	$text = $MSG['30_0212'];
}

$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'SITEURL' => $system->SETTINGS['siteurl'],
		'TEXT' => $text,
		'MYVERSION' => $myversion,
		'REALVERSION' => $realversion
		));

$template->set_filenames(array(
		'body' => 'checkversion.tpl'
		));
$template->display('body');
?>
