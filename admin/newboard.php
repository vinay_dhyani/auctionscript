<?php
/***************************************************************************
 *   ZeeAuctions Ebay Clone
 *   site					: http://www.zeeauctions.com/
 ***************************************************************************/

/***************************************************************************
 *   This program is distributed under the GNU General Public License v2.
 ***************************************************************************/

define('InAdmin', 1);
$current_page = 'contents';
include '../includes/common.inc.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

unset($ERR);

// Insert new message board
if (isset($_POST['action']) && $_POST['action'] == 'insert')
{
	if (empty($_POST['name']) || empty($_POST['msgstoshow']) || empty($_POST['active']))
	{
		$ERR = $ERR_047;
	}
	elseif (!is_numeric($_POST['msgstoshow']))
	{
		$ERR = $ERR_5000;
	}
	elseif (intval($_POST['msgstoshow'] == 0))
	{
		$ERR = $ERR_5001;
	}
	else
	{
		$query = "INSERT INTO " . $DBPrefix . "community VALUES
		(NULL, '" . $system->cleanvars($_POST['name']) . "', 0, 0, " . intval($_POST['msgstoshow']) . ", " . intval($_POST['active']) . " )";
		$system->check_mysql(mysql_query($query), $query, __LINE__, __FILE__);
		header('location: boards.php');
		exit;
	}
}

$template->assign_vars(array(
		'SITEURL' => $system->SETTINGS['siteurl'],
		'ERROR' => (isset($ERR)) ? $ERR : '',

		'NAME' => (isset($_POST['name'])) ? $_POST['name'] : '',
		'MSGTOSHOW' => (isset($_POST['msgstoshow'])) ? $_POST['msgstoshow'] : '',
		'B_ACTIVE' => ((isset($_POST['active']) && $_POST['active'] == 1) || !isset($_POST['active'])),
		'B_DEACTIVE' => (isset($_POST['active']) && $_POST['active'] == 2)
		));

$template->set_filenames(array(
		'body' => 'newboard.tpl'
		));
$template->display('body');

?>