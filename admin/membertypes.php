<?php
/***************************************************************************
 *   ZeeAuctions Ebay Clone
 *   site					: http://www.zeeauctions.com/
 ***************************************************************************/

/***************************************************************************
 *   This program is distributed under the GNU General Public License v2.
 ***************************************************************************/

define('InAdmin', 1);
$current_page = 'settings';
include '../includes/common.inc.php';
include $include_path . 'functions_admin.php';
include $include_path . 'functions_rebuild.inc.php';
include $include_path . 'membertypes.inc.php';
include 'loggedin.inc.php';

unset($ERR);

if (isset($_POST['action']) && $_POST['action'] = 'update')
{
	$old_membertypes = $_POST['old_membertypes'];
	$new_membertypes = $_POST['new_membertypes'];
	$new_membertype = $_POST['new_membertype'];

	// delete with the deletes
	if (isset($_POST['delete']) && is_array($_POST['delete']))
	{
		$idslist = implode(',', $_POST['delete']);
		$query = "DELETE FROM " . $DBPrefix . "membertypes WHERE id IN (" . $idslist . ")";
		$system->check_mysql(mysql_query($query), $query, __LINE__, __FILE__);
	}

	// now update everything else
	if (is_array($old_membertypes))
	{
		foreach ($old_membertypes as $id => $val)
		{
			if ( $val != $new_membertypes[$id])
			{
				$query = "UPDATE " . $DBPrefix . "membertypes SET
						feedbacks = '" . $new_membertypes[$id]['feedbacks'] . "', 
						icon = '" . $system->cleanvars($new_membertypes[$id]['icon']) . "' 
						WHERE id = " . $id;
				$system->check_mysql(mysql_query($query), $query, __LINE__, __FILE__);
			}
		}
	}

	// If a new membertype was added, insert it into database
	if (!empty($new_membertype['feedbacks']))
	{
		$query = "INSERT INTO " . $DBPrefix . "membertypes VALUES (NULL, '" . $new_membertype['feedbacks'] . "', '"
				. $new_membertype['membertype'] . "', '" . "', '" . $system->cleanvars($new_membertype['icon']) . "');";
		$system->check_mysql(mysql_query($query), $query, __LINE__, __FILE__);
	}
	rebuild_table_file('membertypes');
	$ERR = $MSG['836'];
}

foreach ($membertypes as $id => $quest)
{
    $template->assign_block_vars('mtype', array(
			'ID' => $id,
			'FEEDBACK' => $quest['feedbacks'],
			'ICON' => $quest['icon']
			));
}

$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : ''
		));
		
$template->set_filenames(array(
		'body' => 'membertypes.tpl'
		));
$template->display('body');

?>