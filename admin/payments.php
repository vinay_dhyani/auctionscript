<?php
/***************************************************************************
 *   ZeeAuctions Ebay Clone
 *   site					: http://www.zeeauctions.com/
 ***************************************************************************/

/***************************************************************************
 *   This program is distributed under the GNU General Public License v2.
 ***************************************************************************/

define('InAdmin', 1);
$current_page = 'settings';
include '../includes/common.inc.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

unset($ERR);

function ToBeDeleted($index)
{
	if (!isset($_POST['delete']))
		return false;

	$i = 0;
	while ($i < count($_POST['delete']))
	{
		if ($_POST['delete'][$i] == $index) return true;
		$i++;
	}
	return false;
}


if (isset($_POST['action']) && $_POST['action'] == 'update')
{
	// Build new payments array
	$rebuilt_array = array();
	for ($i = 0; $i < count($_POST['new_payments']); $i++)
	{
		if (!ToBeDeleted($i) && strlen($_POST['new_payments'][$i]) != 0)
		{
			$rebuilt_array[] = $_POST['new_payments'][$i];
		}
	}

	$system->SETTINGS['payment_options'] = serialize($rebuilt_array);
	$query = "UPDATE " . $DBPrefix . "settings SET
			payment_options = '" . $system->SETTINGS['payment_options'] . "'";
	$system->check_mysql(mysql_query($query), $query, __LINE__, __FILE__);
	$ERR = $MSG['093'];
}

$payment_options = unserialize($system->SETTINGS['payment_options']);
foreach ($payment_options as $k => $v)
{
	$template->assign_block_vars('payments', array(
			'PAYMENT' => $v,
			'ID' => $k
			));
}


$template->assign_vars(array(
		'SITEURL' => $system->SETTINGS['siteurl'],
		'ERROR' => (isset($ERR)) ? $ERR : ''
		));

$template->set_filenames(array(
		'body' => 'payments.tpl'
		));
$template->display('body');

?>