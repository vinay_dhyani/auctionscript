<?php
/***************************************************************************
 *   ZeeAuctions Ebay Clone
 *   site					: http://www.zeeauctions.com/
 ***************************************************************************/

/***************************************************************************
 *   This program is distributed under the GNU General Public License v2.
 ***************************************************************************/

define('InAdmin', 1);
$current_page = 'contents';
include '../includes/common.inc.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

unset($ERR);

if (isset($_POST['delete']) && is_array($_POST['delete']))
{
	foreach ($_POST['delete'] as $val)
	{
		$query = "DELETE FROM " . $DBPrefix . "faqs WHERE id = " . $val;
		$system->check_mysql(mysql_query($query), $query, __LINE__, __FILE__);
		$query = "DELETE FROM " . $DBPrefix . "faqs_translated WHERE id = " . $val;
		$system->check_mysql(mysql_query($query), $query, __LINE__, __FILE__);
	}
}

// Get data from the database
$query = "SELECT id, category FROM " . $DBPrefix . "faqscategories";
$res = mysql_query($query);
$system->check_mysql($res, $query, __LINE__, __FILE__);
while ($row = mysql_fetch_assoc($res))
{
	$template->assign_block_vars('cats', array(
			'CAT' => $row['category']
			));

	$query = "SELECT id, question FROM " . $DBPrefix . "faqs WHERE category = " . $row['id'];
	$cat_res = mysql_query($query);
	$system->check_mysql($cat_res, $query, __LINE__, __FILE__);
	while ($cat_row = mysql_fetch_assoc($cat_res))
	{
		$template->assign_block_vars('cats.faqs', array(
				'ID' => $cat_row['id'],
				'FAQ' => $cat_row['question']
				));
	}
}

$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : ''
		));

$template->set_filenames(array(
		'body' => 'faqs.tpl'
		));
$template->display('body');

?>