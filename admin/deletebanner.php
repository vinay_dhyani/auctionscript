<?php
/***************************************************************************
 *   ZeeAuctions Ebay Clone
 *   site					: http://www.zeeauctions.com/
 ***************************************************************************/

/***************************************************************************
 *   This program is distributed under the GNU General Public License v2.
 ***************************************************************************/

define('InAdmin', 1);
include '../includes/common.inc.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

if (!isset($_GET['banner']) || empty($_GET['banner']))
{
	header('location: managebanners.php');
	exit;
}

$banner = $_GET['banner'];

$query = "SELECT name, user FROM " . $DBPrefix . "banners WHERE id = " . $banner;
$res = mysql_query($query);
$system->check_mysql($res, $query, __LINE__, __FILE__);
$bannername = mysql_result($res, 0, 'name');
$banneruser = mysql_result($res, 0, 'user');


$query = "DELETE FROM " . $DBPrefix . "banners WHERE id = " . $banner;
$system->check_mysql(mysql_query($query), $query, __LINE__, __FILE__);
$query = "DELETE FROM " . $DBPrefix . "bannerscategories WHERE banner = " . $banner;
$system->check_mysql(mysql_query($query), $query, __LINE__, __FILE__);
$query = "DELETE FROM " . $DBPrefix . "bannerskeywords WHERE banner = " . $banner;
$system->check_mysql(mysql_query($query), $query, __LINE__, __FILE__);
@unlink($upload_path . 'banners/' . $banneruser . '/' . $bannername);

// Redirect
header('location: userbanners.php?id=' . $banneruser);
?>