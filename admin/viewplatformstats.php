<?php
/***************************************************************************
 *   ZeeAuctions Ebay Clone
 *   site					: http://www.zeeauctions.com/
 ***************************************************************************/

/***************************************************************************
 *   This program is distributed under the GNU General Public License v2.
 ***************************************************************************/

define('InAdmin', 1);
$current_page = 'stats';
include '../includes/common.inc.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

// Retrieve data
$query = "SELECT * FROM " . $DBPrefix . "currentplatforms WHERE month = " . date('n') . " AND year = " . date('Y') . " ORDER BY counter DESC";
$res = mysql_query($query);
$system->check_mysql($res, $query, __LINE__, __FILE__);

$MAX = 0;
$TOTAL = 0;
while ($row = mysql_fetch_assoc($res))
{
	$PLATFORMS[$row['platform']] = $row['counter'];
	$TOTAL = $TOTAL + $row['counter'];

	if ($row['counter'] > $MAX)
	{
		$MAX = $row['counter'];
	}
}

if (is_array($PLATFORMS))
{
	foreach ($PLATFORMS as $k => $v)
	{
		$template->assign_block_vars('sitestats', array(
			'PLATFORM' => $k,
			'NUM' => $PLATFORMS[$k],
			'WIDTH' => ($PLATFORMS[$k] * 100) / $MAX,
			'PERCENTAGE' => ceil(intval($PLATFORMS[$k] * 100 / $TOTAL))
			));
	}
}

$template->assign_vars(array(
		'SITENAME' => $system->SETTINGS['sitename'],
		'STATSMONTH' => date('F Y')
		));

$template->set_filenames(array(
		'body' => 'viewplatformstats.tpl'
		));
$template->display('body');
?>
