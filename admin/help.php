<div style="margin-bottom:20px;">
    <b>Get help</b>
    <ul>
        <li><a href="http://www.zeeauctions.com/forums/" target="_blank">Support forums</a></li>
        <li><a href="http://www.zeeauctions.com/wiki/Category:Documentation" target="_blank">Online documentation</a></li>
        <li><a href="http://www.zeeauctions.com/wiki/IRC" target="_blank">IRC Support</a></li>
    </ul>
</div>

<div style="margin-bottom:20px;">
    <b>Edit ZeeAuctions</b>
    <ul>
        <li><a href="http://www.zeeauctions.com/mods.php?do=themes" target="_blank">Download Themes</a></li>
        <li><a href="http://www.zeeauctions.com/mods.php?do=mods" target="_blank">Download Mods</a></li>
    </ul>
</div>

<b>Support ZeeAuctions</b>
<ul>
    <li><a href="http://www.zeeauctions.com/forums/project.php?do=issuelist&projectid=1&issuetypeid=bug" target="_blank">Submit a bug</a></li>
    <li><a href="http://www.zeeauctions.com/forums/project.php?do=issuelist&projectid=1&issuetypeid=feature" target="_blank">Suggest a feature</a></li>
    <li><a href="http://www.zeeauctions.com/donate.php" target="_blank">Donate</a></li>
</ul>