<?php
/***************************************************************************
 *   ZeeAuctions Ebay Clone
 *   site					: http://www.zeeauctions.com/
 ***************************************************************************/

/***************************************************************************
 *   This program is distributed under the GNU General Public License v2.
 ***************************************************************************/

define('InAdmin', 1);
$current_page = 'tools';
include '../includes/common.inc.php';
include $include_path . 'functions_admin.php';
include 'loggedin.inc.php';

unset($ERR);

if (isset($_POST['action']) && $_POST['action'] == 'clearlog')
{
	$file = @fopen($logPath . 'error.log', 'w+');
	fclose($file);
	$ERR = $MSG['889'];
}

$data = file_get_contents($logPath . 'error.log');

if ($data == '')
{
	$data = $MSG['888'];
}
else
{
	$data = str_replace("\n", '<br>', $data);
	$data = preg_replace('/(\d{2}-\d{2}-\d{4}, \d{2}:\d{2}:\d{2}::)/s', '<b>$1</b>', $data);
}

$template->assign_vars(array(
		'ERROR' => (isset($ERR)) ? $ERR : '',
		'SITEURL' => $system->SETTINGS['siteurl'],
		'ERRORLOG' => $data
		));

$template->set_filenames(array(
		'body' => 'errorlog.tpl'
		));
$template->display('body');
?>
