<?php
/***************************************************************************
 *   ZeeAuctions Ebay Clone
 *   site					: http://www.zeeauctions.com/
 ***************************************************************************/

/***************************************************************************
 *   This program is distributed under the GNU General Public License v2.
 ***************************************************************************/
 
function getmainpath()
{
	$path = getcwd();
	$path = str_replace('install', '', $path);
	return $path;
}

function getdomainpath()
{
	$path = 'http://' . dirname($_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
	if (strlen($path) < 12)
	{
		$path = 'http://' . dirname($_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']);
	}
	$path = str_replace('install', '', $path);
	return $path;
}

function makeconfigfile($contents, $main_path)
{
	$filename = $main_path . 'includes/config.inc.php';
	$altfilename = $main_path . 'includes/config.inc.php.new';

	if (!file_exists($filename))
	{
		if (file_exists($altfilename))
		{
			rename($altfilename, $filename);
		}
		else
		{
			touch($filename);
		}
	}

	@chmod($filename, 0777);

	if (is_writable($filename))
	{
		if (!$handle = fopen($filename, 'w')) 
		{
			$return = false;
		}
		else
		{
			if (fwrite($handle, $contents) === false)
			{
				$return = false;
			}
			else
			{
				$return = true;
			}
		}
		fclose($handle);
	}
	else
	{
		$return = false;
	}

	return $return;
}

function print_header($update)
{
	global $thisversion;
	if ($update)
	{
		global $_SESSION, $myversion;
		if (!isset($_SESSION['oldversion']))
		{
			$_SESSION['oldversion'] = $myversion;
		}

		return '<h3 align="center">ZeeAuctions Updater, v' . $_SESSION['oldversion'] . ' to v' . $thisversion . '</h3> <br>';
	}
	else
	{
		return '<h3 align="center">ZeeAuctions Installer v' . $thisversion . '</h3><br>';
	}
}

function check_version()
{
	global $DBPrefix, $settings_version;

	// check if using an old version
	if (!isset($settings_version) || empty($settings_version))
	{
		$version = file_get_contents('../includes/version.txt') or die('error');
		$query = "ALTER TABLE `" . $DBPrefix . "settings` ADD `version` varchar(10) NOT NULL default '" . $version . "'";
		$res = @mysql_query($query);
		return $version;
	}

	return $settings_version;
}

function check_installation()
{
	global $DBPrefix, $settings_version, $main_path;

	include '../includes/config.inc.php';
	@mysql_connect($DbHost, $DbUser, $DbPassword);
	@mysql_select_db($DbDatabase);
	$DBPrefix = (isset($DBPrefix)) ? $DBPrefix : '';
	// check zeeauctions install...
	$query = "SELECT * FROM `" . $DBPrefix . "settings`";
	$res = @mysql_query($query);
	if ($res != false)
	{
		$settings_version = @mysql_result($res, 0, 'version');
		return true;
	}
	else
	{
		return false;
	}
}

function this_version()
{
	$string = file_get_contents('thisversion.txt') or die('error');
	return $string;
}

function show_config_table($fresh = true)
{
	$main_path = getmainpath();

	$data = '<form name="form1" method="post" action="?step=1">';
	$data .= '<table align="center" cellspacing="1" border="0" style="border-collapse:collapse;" bgcolor="#CCCCCC" cellpadding="6" >';
	$data .= '<tr>';
	$data .= '	<td width="140">SITE URL</td>';
	$data .= '	<td width="108">';
	$data .= '	  <input type="text" name="URL" id="textfield" value="' . getdomainpath() . '">';
	$data .= '	</td>';
	$data .= '	<td rowspan="2">';
	$data .= '	   <br>';
	$data .= '	 <br> ';
	$data .= '	</td>';
	$data .= '  </tr>';
	$data .= '  <tr>';
	$data .= '	<td>Root Path</td>';
	$data .= '	<td>';
	$data .= '	  <input type="text" name="mainpath" id="textfield" value="' . $main_path . '">';
	$data .= '	</td>';
	$data .= '</tr>';
	if ($fresh)
	{
		$data .= '<tr>';
		$data .= '	<td>Admin Email</td>';
		$data .= '	<td>';
		$data .= '	  <input type="text" name="EMail" id="textfield">';
		$data .= '	</td>';
		$data .= '	<td></td>';
		$data .= '</tr>';
	}
	$data .= '<tr>';
	$data .= '	<td>Database Host</td>';
	$data .= '	<td>';
	$data .= '	  <input type="text" name="DBHost" id="textfield" value="localhost">';
	$data .= '	</td>';
	$data .= '	<td> </td>';
	$data .= '  </tr>';
	$data .= '  <tr>';
	$data .= '	<td>Database Username</td>';
	$data .= '	<td>';
	$data .= '	  <input type="text" name="DBUser" id="textfield">';
	$data .= '	</td>';
	$data .= '	<td rowspan="3"></td>';
	$data .= '  </tr>';
	$data .= '  <tr>';
	$data .= '	<td>Database Password</td>';
	$data .= '	<td>';
	$data .= '	  <input type="text" name="DBPass" id="textfield">';
	$data .= '	</td>';
	$data .= '  </tr>';
	$data .= '  <tr>';
	$data .= '	<td>Database Name</td>';
	$data .= '	<td>';
	$data .= '	  <input type="text" name="DBName" id="textfield">';
	$data .= '	</td>';
	$data .= '  </tr>';
	$data .= '  <tr>';
	$data .= '	<td>Database Prefix</td>';
	$data .= '	<td>';
	$data .= '	  <input type="text" name="DBPrefix" id="textfield" value="zee_">';
	$data .= '	</td>';
	$data .= '	<td>Recommended</td>';
	$data .= '</tr>';
	if ($fresh)
	{
		$data .= '<tr>';
		$data .= '	<td>Import Categories</td>';
		$data .= '	<td>';
		$data .= '	  <input type="checkbox" name="importcats" id="checkbox" checked="checked">';
		$data .= '	</td>';
		$data .= '	<td>Recommended.</td>';
		$data .= '</tr>';
	}
	$data .= '</table>';

	if ($fresh)
	{
		$data .= '<br>';
		$data .= '<table align="center" cellspacing="1" border="0" style="border-collapse:collapse;" bgcolor="#CCCCCC" cellpadding="6" width="420px">';
		$data .= '<tr>';
		$data .= '	<td colspan="2"><b>File Checks:</b></td>';
		$data .= '</tr>';
		$directories = array(
			'cache/',
			'uploaded/',
			'uploaded/banners/',
			'uploaded/cache/'
			);

		umask(0);

		$passed = true;
		foreach ($directories as $dir)
		{
			$exists = $write = false;

			// Try to create the directory if it does not exist
			if (!file_exists($main_path . $dir))
			{
				@mkdir($main_path . $dir, 0777);
				@chmod($main_path . $dir, 0777);
			}

			// Now really check
			if (file_exists($main_path . $dir) && is_dir($main_path . $dir))
			{
				$exists = true;
			}

			// Now check if it is writable by storing a simple file
			$fp = @fopen($main_path . $dir . 'test_lock', 'wb');
			if ($fp !== false)
			{
				$write = true;
			}
			@fclose($fp);

			@unlink($main_path . $dir . 'test_lock');

			if (!$exists || !$write)
			{
				$passed = false;
			}

			$data .= '<tr><td>' . $dir . ':</td><td>';
			$data .= ($exists) ? '<strong style="color:green">Found</strong>' : '<strong style="color:red">Not Found</strong>';
			$data .= ($write) ? ', <strong style="color:green">Writable</strong>' : (($exists) ? ', <strong style="color:red">Unwritable</strong>' : '');
			$data .= '</tr>';
		}

		$directories = array(
			'includes/config.inc.php',
			'includes/countries.inc.php',
			'includes/currencies.php',
			'includes/membertypes.inc.php',
			'language/EN/categories.inc.php',
			'language/EN/categories_select_box.inc.php'
			);

		foreach ($directories as $dir)
		{
			$write = $exists = true;
			if (file_exists($main_path . $dir))
			{
				if (!@is_writable($main_path . $dir))
				{
					$write = false;
				}
			}
			else
			{
				$write = $exists = false;
			}

			if (!$exists || !$write)
			{
				$passed = false;
			}

			$data .= '<tr><td>' . $dir . ':</td><td>';
			$data .= ($exists) ? '<strong style="color:green">Found</strong>' : '<strong style="color:red">Not Found</strong>';
			$data .= ($write) ? ', <strong style="color:green">Writable</strong>' : (($exists) ? ', <strong style="color:red">Unwritable</strong>' : '');
			$data .= '</tr>';
		}

		$data .= '<tr><td>GD Support:</td><td>';
		$data .= (extension_loaded('gd') && function_exists('gd_info')) ? '<strong style="color:green">Found</strong>' : '<strong style="color:red">Not Found</strong>';
		$data .= '</tr>';

		$data .= '<tr><td>BC Math Support:</td><td>';
		$data .= (extension_loaded('bcmath')) ? '<strong style="color:green">Found</strong>' : '<strong style="color:red">Not Found</strong>';
		$data .= '</tr>';

		$data .= '</table>';
	}

	$data .= '<br>';
	if ($fresh && $passed || !$fresh)
	{
		$data .= '<input class="button" type="submit" value="INSTALL NOW"> <br><br>';
	}
	$data .= '</form>';

	return $data;
}

function search_cats($parent_id, $level)
{
	global $DBPrefix, $catscontrol;
	$root = $catscontrol->get_virtual_root();
	$tree = $catscontrol->display_tree($root['left_id'], $root['right_id'], '|___');
	foreach ($tree as $k => $v)
	{
		$catstr .= ",\n" . $k . " => '" . $v . "'";
	}
	return $catstr;
}

function rebuild_cat_file()
{
	global $system, $main_path, $DBPrefix;
	$query = "SELECT cat_id, cat_name, parent_id FROM " . $DBPrefix . "categories ORDER BY cat_name";
	$result = mysql_query($query);
	$cats = array();
	while ($catarr = mysql_fetch_array($result))
	{
		$cats[$catarr['cat_id']] = $catarr['cat_name'];
		$allcats[] = $catarr;
	}

	$output = "<?php\n";
	$output.= "$" . "category_names = array(\n";

	$num_rows = count($cats);

	$i = 0;
	foreach ($cats as $k => $v)
	{
		$output .= "$k => '$v'";
		$i++;
		if ($i < $num_rows)
			$output .= ",\n";
		else
			$output .= "\n";
	}

	$output .= ");\n\n";

	$output .= "$" . "category_plain = array(\n0 => ''";

	$output .= search_cats(0, 0);

	$output .= ");\n?>";

	$handle = fopen ($main_path . 'language/' . $system->SETTINGS['defaultlanguage'] . '/categories.inc.php', 'w');
	fputs($handle, $output);
}
?>