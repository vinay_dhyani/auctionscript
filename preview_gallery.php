<?php
/***************************************************************************
 *   ZeeAuctions Ebay Clone
 *   site					: http://www.zeeauctions.com/
 ***************************************************************************/

/***************************************************************************
 *   This program is distributed under the GNU General Public License v2.
 ***************************************************************************/
 
include 'includes/common.inc.php';

$UPLOADED_PICTURES = $_SESSION['UPLOADED_PICTURES'];
$img = $_GET['img'];

$template->assign_vars(array(
		'SITEURL' => $system->SETTINGS['siteurl'],
		'IMG' => $uploaded_path . session_id() . '/' . $UPLOADED_PICTURES[$img]
		));
$template->set_filenames(array(
		'body' => 'preview_gallery.tpl'
		));
$template->display('body');
?>
