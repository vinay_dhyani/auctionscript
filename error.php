<?php
/***************************************************************************
 *   ZeeAuctions Ebay Clone
 *   site					: http://www.zeeauctions.com/
 ***************************************************************************/

/***************************************************************************
 *   This program is distributed under the GNU General Public License v2.
 ***************************************************************************/

$error = true;
include 'includes/common.inc.php';

$template->assign_vars(array(
		'ERROR' => print_r($_SESSION['SESSION_ERROR'], true),
		'DEBUGGING' => false, // set to true when trying to fix the script
		'ERRORTXT' => $system->SETTINGS['errortext']
		));

include 'header.php';
$template->set_filenames(array(
		'body' => 'error.tpl'
		));
$template->display('body');
include 'footer.php';
?>
