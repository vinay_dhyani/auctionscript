<?php
/***************************************************************************
 *   ZeeAuctions Ebay Clone
 *   site					: http://www.zeeauctions.com/
 ***************************************************************************/

/***************************************************************************
 *   This program is distributed under the GNU General Public License v2.
 ***************************************************************************/

include 'includes/common.inc.php';

// Handle banners clickthrough
$URL = trim($_GET['url']);

// Update clickthrough counter in the database
$query = "UPDATE " . $DBPrefix . "banners set clicks = clicks + 1 WHERE id=" . intval($_GET['banner']);
$system->check_mysql(mysql_query($query), $query, __LINE__, __FILE__);

// Redirect
header('location: ' . $URL);
exit;
?>
