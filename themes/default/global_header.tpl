<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en" dir="{DOCDIR}">
<head>
<title>{PAGE_TITLE}</title>
<meta http-equiv="Content-Type" content="text/html; charset={CHARSET}">
<meta name="description" content="{DESCRIPTION}">
<meta name="keywords" content="{KEYWORDS}">
<meta name="generator" content="ZeeAuctions">
 


<link href="{INCURL}themes/{THEME}/css/style.css" rel="stylesheet" type="text/css" />
<link href="{INCURL}themes/{THEME}/css/nav-menus.css" rel="stylesheet" type="text/css" />
<link href="{INCURL}themes/{THEME}/css/jqtransform.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="{INCURL}themes/{THEME}/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="{INCURL}themes/{THEME}/js/stuHover.js"></script>
<script type="text/javascript" src="{INCURL}themes/{THEME}/js/jquery.flow.1.2.auto.js"></script>
<script type="text/javascript" src="{INCURL}themes/{THEME}/js/tabs.js"></script>
<script type="text/javascript" src="{INCURL}themes/{THEME}/js/toggler.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("#myController").jFlow({
		slides: "#slides",
		controller: ".jFlowControl", // must be class, use . sign
		slideWrapper : "#jFlowSlide", // must be id, use # sign
		selectedWrapper: "jFlowSelected",  // just pure text, no sign
		auto: true,		//auto change slide, default true
		width: "554px",
		height: "228px",
		duration: 600,
		prev: ".jFlowPrev", // must be class, use . sign
		next: ".jFlowNext" // must be class, use . sign
	});
});
</script>
<link rel="stylesheet" type="text/css" href="{INCURL}themes/{THEME}/style.css">
<link rel="stylesheet" type="text/css" href="{INCURL}themes/{THEME}/jquery.lightbox.css" media="screen">
<link rel="stylesheet" type="text/css" href="{INCURL}includes/calendar.css">

<link rel="alternate" type="application/rss+xml" title="{L_924}" href="{SITEURL}rss.php?feed=1">
<link rel="alternate" type="application/rss+xml" title="{L_925}" href="{SITEURL}rss.php?feed=2">
<link rel="alternate" type="application/rss+xml" title="{L_926}" href="{SITEURL}rss.php?feed=3">
<link rel="alternate" type="application/rss+xml" title="{L_927}" href="{SITEURL}rss.php?feed=4">
<link rel="alternate" type="application/rss+xml" title="{L_928}" href="{SITEURL}rss.php?feed=5">
<link rel="alternate" type="application/rss+xml" title="{L_929}" href="{SITEURL}rss.php?feed=6">
<link rel="alternate" type="application/rss+xml" title="{L_930}" href="{SITEURL}rss.php?feed=7">
<link rel="alternate" type="application/rss+xml" title="{L_931}" href="{SITEURL}rss.php?feed=8">

<script type="text/javascript" src="{INCURL}loader.php?js={JSFILES}"></script>
<!-- IF LOADCKEDITOR -->
	<script type="text/javascript" src="{INCURL}ckeditor/ckeditor.js"></script>
<!-- ENDIF -->

<script type="text/javascript">
$(document).ready(function() {
	$('a.new-window').click(function(){
		var posY = ($(window).height()-550)/2;
		var posX = ($(window).width())/2;
		window.open(this.href, this.alt, "toolbar=0,location=0,directories=0,scrollbars=1,screenX="+posX+",screenY="+posY+",status=0,menubar=0,width=550,height=550");
		return false;
	});
	var currenttime = '{ACTUALDATE}';
	var serverdate = new Date(currenttime);
	function padlength(what){
		var output=(what.toString().length==1)? "0"+what : what;
		return output;
	}
	function displaytime(){
		serverdate.setSeconds(serverdate.getSeconds()+1)
		var timestring=padlength(serverdate.getHours())+":"+padlength(serverdate.getMinutes())+":"+padlength(serverdate.getSeconds());
		$("#servertime").html(timestring);
	}
	setInterval(displaytime, 1000);
	$(function() {
		$('#gallery a').lightBox();
	});
});
</script>
</head>
<body> <div class="wrapper rounded-top rounded-bottom">
<div class="wrap" id="three_columns">
  <div id="head">
    {LOGO}
    <div class="basket"><!-- IF B_LOGGED_IN -->{L_200} {YOURUSERNAME}! <a href="{SSLURL}logout.php?">{L_245}</a><!-- ENDIF -->
       
    </div>
<div id="search">
  <form name="search" action="{SITEURL}search.php" method="get">
              &nbsp; &nbsp; &nbsp;   {SELECTION_BOX}
                <input type="text" name="q" size="50" value="{Q}">
        <input type="submit" name="sub" value="" class="button">
                <a href="{SITEURL}adsearch.php">{L_464}</a>
      </form>
  </div>
  </div>  
  <div id="main_nav">
    <div class="user_nav"> <a href="{SITEURL}register.php?"><img src="{INCURL}themes/{THEME}/images/register.png" title="Free Registration" /></a></div>
    <ul id="nav">
      <li class="top"><a href="{SITEURL}index.php?" class="top_link"><span>{L_166}</span></a></li>
      <li class="top"><a href="{SITEURL}contents.php?show=aboutus" class="top_link"><span>{L_5085}</span></a></li>
      
      <!-- IF B_CAN_SELL -->
	  <li class="top"><a href="{SITEURL}select_category.php?" class="top_link"><span>{L_028}</span></a></li>
<!-- ENDIF -->
<!-- IF B_LOGGED_IN -->
      <li class="top"><a href="{SITEURL}user_menu.php?" class="top_link"><span>{L_622}</span></a></li>
      <li class="top"><a href="{SSLURL}logout.php?" class="top_link"><span>{L_245}</span></a></li>
<!-- ELSE -->
      <li class="top"><a href="{SSLURL}register.php?" class="top_link"><span>{L_235}</span></a></li>
      <li class="top"><a href="{SSLURL}user_login.php?" class="top_link"><span>{L_052}</span></a></li>
<!-- ENDIF -->
<!-- IF B_BOARDS -->
	  <li class="top"><a href="{SITEURL}boards.php" class="top_link"><span>{L_5030}</span></a></li>
<!-- ENDIF -->
      
      
       
      
      <li class="top"><a href="javascript: void(0)" class="top_link"><span class="down">{L_5085b}</span></a>
        <ul class="sub">
          <li><a href="{SITEURL}faqs.php" alt="faqs" class="new-window">{L_148}</a></li>
          <li><a href="{SITEURL}contents.php?show=priv" >{L_401}</a></li>
          <li><a href="{SITEURL}contents.php?show=terms">{L_5086}</a></li>
          <li><a href="{SITEURL}contents.php?show=aboutus">{L_5085a}</a></li>
           
        </ul>
      </li>
    </ul>
  </div>
</div>



 