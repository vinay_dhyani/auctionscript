<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en" dir="{DOCDIR}">
<head>
<title>{PAGE_TITLE}</title>
<meta http-equiv="Content-Type" content="text/html; charset={CHARSET}">
<meta name="description" content="{DESCRIPTION}">
<meta name="keywords" content="{KEYWORDS}">
<meta name="generator" content="ZeeAuctions">

 
<link href="{INCURL}themes/{THEME}/css/style.css" rel="stylesheet" type="text/css" />
<link href="{INCURL}themes/{THEME}/css/nav-menus.css" rel="stylesheet" type="text/css" />
<link href="{INCURL}themes/{THEME}/css/jqtransform.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="{INCURL}themes/{THEME}/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="{INCURL}themes/{THEME}/js/stuHover.js"></script>
<script type="text/javascript" src="{INCURL}themes/{THEME}/js/jquery.flow.1.2.auto.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("#myController").jFlow({
		slides: "#slides",
		controller: ".jFlowControl", // must be class, use . sign
		slideWrapper : "#jFlowSlide", // must be id, use # sign
		selectedWrapper: "jFlowSelected",  // just pure text, no sign
		auto: true,		//auto change slide, default true
		width: "554px",
		height: "228px",
		duration: 600,
		prev: ".jFlowPrev", // must be class, use . sign
		next: ".jFlowNext" // must be class, use . sign
	});
});
</script>
<link rel="stylesheet" type="text/css" href="{INCURL}themes/{THEME}/style.css">
<link rel="stylesheet" type="text/css" href="{INCURL}themes/{THEME}/jquery.lightbox.css" media="screen">
<link rel="stylesheet" type="text/css" href="{INCURL}includes/calendar.css">

<link rel="alternate" type="application/rss+xml" title="{L_924}" href="{SITEURL}rss.php?feed=1">
<link rel="alternate" type="application/rss+xml" title="{L_925}" href="{SITEURL}rss.php?feed=2">
<link rel="alternate" type="application/rss+xml" title="{L_926}" href="{SITEURL}rss.php?feed=3">
<link rel="alternate" type="application/rss+xml" title="{L_927}" href="{SITEURL}rss.php?feed=4">
<link rel="alternate" type="application/rss+xml" title="{L_928}" href="{SITEURL}rss.php?feed=5">
<link rel="alternate" type="application/rss+xml" title="{L_929}" href="{SITEURL}rss.php?feed=6">
<link rel="alternate" type="application/rss+xml" title="{L_930}" href="{SITEURL}rss.php?feed=7">
<link rel="alternate" type="application/rss+xml" title="{L_931}" href="{SITEURL}rss.php?feed=8">

<script type="text/javascript" src="{INCURL}loader.php?js={JSFILES}"></script>
<!-- IF LOADCKEDITOR -->
	<script type="text/javascript" src="{INCURL}ckeditor/ckeditor.js"></script>
<!-- ENDIF -->

<script type="text/javascript">
$(document).ready(function() {
	$('a.new-window').click(function(){
		var posY = ($(window).height()-550)/2;
		var posX = ($(window).width())/2;
		window.open(this.href, this.alt, "toolbar=0,location=0,directories=0,scrollbars=1,screenX="+posX+",screenY="+posY+",status=0,menubar=0,width=550,height=550");
		return false;
	});
	var currenttime = '{ACTUALDATE}';
	var serverdate = new Date(currenttime);
	function padlength(what){
		var output=(what.toString().length==1)? "0"+what : what;
		return output;
	}
	function displaytime(){
		serverdate.setSeconds(serverdate.getSeconds()+1)
		var timestring=padlength(serverdate.getHours())+":"+padlength(serverdate.getMinutes())+":"+padlength(serverdate.getSeconds());
		$("#servertime").html(timestring);
	}
	setInterval(displaytime, 1000);
	$(function() {
		$('#gallery a').lightBox();
	});
});
</script>
</head> <body>

<div class="wrap" id="three_columns">
<div class="content clearfix">
  <div class="left_column">
    <div class="product_menu">
      <h2><span>Product Categories</span></h2> 
        <div class="smallpadding">
        <ul>
<!-- BEGIN cat_list -->
            <li>
                <span style="background-color:{cat_list.COLOUR}">
                <a href="browse.php?id={cat_list.ID}">{cat_list.IMAGE}{cat_list.NAME}</a> {cat_list.CATAUCNUM}
                </span>
            </li>
<!-- END cat_list -->
        </ul>
        <a href="{SITEURL}browse.php?id=0">{L_277}</a>
    </div></div>
    <div class="box newsletter_box">
      <div class="payment_box">
        <h2>{L_5085c}</h2>
      <a href="http://www.zeescripts.com/go/paypal" target="_blank"><img src="{INCURL}themes/{THEME}/images/payment_options.png" width="167" height="71" alt="Free Paypal Account" /></a></div>
        <p>&nbsp;</p>
        
      </div>
  </div>
    <div class="main_content">
      <div class="jflow-content-slider">
        <div id="slides">
          <div class="slide-wrapper">{BANNER}</div>

        </div>
         
        <div class="clear"></div>
      </div>
      <div class="products_box featured">
        <h2 class="box_title"><a href="{SITEURL}browse.php?id=0"><span>{L_5085d}</span></a> {L_5085e}</h2>
        <div class="products_content"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="smallpadding">
<tr>
	<td valign="top">
	  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="maincolum">
	    <tr>
	      <td class="table2">
	        <!-- BEGIN featured -->
	        <div style="float:left;display:block;width:150px;margin:5px;background-color:#FFFEEE;border:#CCCCCC 1px solid;padding:5px;">
	          <div style="display:block;" align="center"><img src="{featured.IMAGE}"></div>
	          <div style="display:block;" align="center"><a href="{SITEURL}item.php?id={featured.ID}">{featured.TITLE}</a><br>{featured.BID}</div>
            </div>
	        <!-- END featured -->
          </td>
        </tr>  
	    <!-- IF B_HOT_ITEMS -->
	    <tr>
	      <td class="titTable4">{L_279}</td>
        </tr>
	    <tr>
	      <td class="table2">
	        <!-- BEGIN hotitems -->
	        <div style="float:left;display:block;width:150px;margin:5px;background-color:#FFFEEE;border:#CCCCCC 1px solid;padding:5px;">
	          <div style="display:block;" align="center"><img src="{hotitems.IMAGE}"></div>
	          <div style="display:block;" align="center"><a href="{SITEURL}item.php?id={hotitems.ID}">{hotitems.TITLE}</a><br>{hotitems.BID}</div>
            </div>
	        <!-- END hotitems -->
          </td>
        </tr>
	    <!-- ENDIF -->
	    <!-- IF B_AUC_LAST -->
	    <tr>
	      <td class="titTable4">{L_278}</td>
        </tr>
	    <tr>
	      <td class="table2">
	        <!-- BEGIN auc_last -->
	        <p style="display:block;" {auc_last.BGCOLOUR}>{auc_last.DATE} <a href="{SITEURL}item.php?id={auc_last.ID}">{auc_last.TITLE}</a></p>
	        <!-- END auc_last -->
          </td>
        </tr>
	    <!-- ENDIF -->
	    <!-- IF B_AUC_ENDSOON -->
	    <tr>
	      <td class="titTable4">{L_280}</td>
        </tr>
	    <tr>
	      <td class="table2">
	        <!-- BEGIN end_soon -->
	        <p style="display:block;" {end_soon.BGCOLOUR}>{end_soon.DATE} <a href="{SITEURL}item.php?id={end_soon.ID}">{end_soon.TITLE}</a></p>
	        <!-- END end_soon -->
          </td>
        </tr>
	    <!-- ENDIF -->
      </table>
</td>

</tr>
</table></div>
      </div>
    </div>
    <div class="right_column">
      
       <td width="20%" valign="top" class="columR">
<!-- IF B_MULT_LANGS -->
    <div class="titTable1 rounded-left">
    	{L_2__0001}
    </div>
    <div class="smallpaddingright">
        {FLAGS}
    </div> 
<!-- ENDIF -->
<!-- IF B_LOGIN_BOX -->
	<!-- IF B_LOGGED_IN -->
    <div class="titTable1 rounded-left">
    	{L_200} {YOURUSERNAME}
    </div>
    <div class="smallpadding">
        <ul>
            <li><a href="{SITEURL}edit_data.php?">{L_244}</a></li>
            <li><a href="{SITEURL}user_menu.php">{L_622}</a></li>
            <li><a href="{SITEURL}logout.php">{L_245}</a></li>
        </ul>
    </div>
	<!-- ELSE -->
    <div class="titTable1 rounded-left">
    	{L_221}
    </div>
    <div class="smallpadding">
        <form name="login" action="{SSLURL}user_login.php" method="post">
        <input type="hidden" name="csrftoken" value="{_CSRFTOKEN}">
            <table width="100%">
            <tr>
                <td width="25%"><label for="username" title="please enter your username">{L_003}</label></td>
                <td width="75%"><input type="text" name="username" id="username" size="10" maxlength="20"></td>
            </tr>
            <tr>
                <td width="25%"><label for="password" title="please enter your password">{L_004}&nbsp;</label></td>
                <td width="75%"><input type="password" name="password" id="password" size="10" maxlength="20"></td>
            </tr>
            </table>
            <p><input type="checkbox" name="rememberme" id="rememberme" value="1"><label for="rememberme">&nbsp;{L_25_0085}</label></p>
            <p align="center"><input type="submit" name="action" value="{L_275}" class="button"></p>
            <p><a href="{SITEURL}forgotpasswd.php">{L_215}</a></p>
        </form>
    </div>
	<!-- ENDIF -->
<!-- ENDIF -->
<!-- IF B_HELPBOX -->
    <div class="titTable1 rounded-left">
    	{L_281}
    </div>
    <div class="smallpadding">
        <ul>
        <!-- BEGIN helpbox -->
            <li><a href="{SITEURL}viewfaqs.php?cat={helpbox.ID}" alt="faqs"  class="new-window">{helpbox.TITLE}</a></li>
        <!-- END helpbox -->
        </ul>
    </div>
<!-- ENDIF -->
<!-- IF B_NEWS_BOX -->
    <div class="titTable1 rounded-left">
    	{L_282}
    </div>
    <div class="smallpadding">
        <ul>
        <!-- BEGIN newsbox -->
            <li>{newsbox.DATE} - <a href="viewnew.php?id={newsbox.ID}">{newsbox.TITLE}</a></li>
        <!-- END newsbox -->
        </ul>
    </div>   
<!-- ENDIF -->
	</td>
  </div>
</div>

