            	<div class="box">
                	<h4 class="rounded-top">{L_25_0018}</h4>
                    <div class="rounded-bottom">
                    	<ul class="menu">
                        	<li><a href="{SITEURL}admin/news.php">{L_516}</a></li>
                        	<li><a href="{SITEURL}admin/aboutus.php">{L_5074}</a></li>
                        	<li><a href="{SITEURL}admin/terms.php">{L_5075}</a></li>
                        	<li><a href="{SITEURL}admin/privacypolicy.php">{L_402}</a></li>
                        </ul>
                    </div>
                </div>
            	<div class="box">
                	<h4 class="rounded-top">{L_5236}</h4>
                    <div class="rounded-bottom">
                    	<ul class="menu">
                        	<li><a href="{SITEURL}admin/faqscategories.php">{L_5230}</a></li>
                        	<li><a href="{SITEURL}admin/newfaq.php">{L_5231}</a></li>
                        	<li><a href="{SITEURL}admin/faqs.php">{L_5232}</a></li>
                        </ul>
                    </div>
                </div>
            	<div class="box">
                	<h4 class="rounded-top">{L_5030}</h4>
                    <div class="rounded-bottom">
                    	<ul class="menu">
                        	<li><a href="{SITEURL}admin/boardsettings.php">{L_5047}</a></li>
                        	<li><a href="{SITEURL}admin/newboard.php">{L_5031}</a></li>
                        	<li><a href="{SITEURL}admin/boards.php">{L_5032}</a></li>
                        </ul>
                    </div>
                </div>
            	<div class="box">
                	<h4 class="rounded-top">Tips and Info</h4>
                    <div class="rounded-bottom">
                    	Here in this section you can add or edit site static content.
                    Add latest news. Add or edit about us page -where you place your contact information or contact form code. You also add or edit your site terms and policies. Plus you also add various categories to your FAQs (Frequently Asked Questions or Help Articles) section. If you want community or message boards you can activate or create boards (forums) for discussions.</div>
                </div>