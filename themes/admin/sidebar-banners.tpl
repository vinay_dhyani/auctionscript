            	<div class="box">
                	<h4 class="rounded-top">{L_25_0011}</h4>
                    <div class="rounded-bottom">
                    	<ul class="menu">
                        	<li><a href="{SITEURL}admin/banners.php">{L_5205}</a></li>
                        	<li><a href="{SITEURL}admin/managebanners.php">{L__0008}</a></li>
                        </ul>
                    </div>
                </div>
            	<div class="box">
                	<h4 class="rounded-top">Tips and Info</h4>
                    <div class="rounded-bottom">
               	      <p>The present theme is designed to display banners at home page featured area - the size width of banner best suited is 554px. If you want the banner to appear at other zone you can edit the home.tpl file and remove the <span class="slide-wrapper">{BANNER}</span> tag and place it in other file like header or footer or wherever you wish. If you want a central banner server with multiple zones of various sizes to place and rotate banners at various zones, you need advanced solution like <a href="http://www.phpadserver.com/" target="_blank">PHP Adserve</a>r script.</p>
               	      <p>Click on the Manage Banners like for the selected user to edit or delete banners. Select all categories or single or few categories by holding ctrl key of your keyboard and select categories.</p>
                    </div>
                </div>