            	<div class="box">
                	<h4 class="rounded-top">{L_239}</h4>
                    <div class="rounded-bottom">
                    	<ul class="menu">
                        	<li><a href="{SITEURL}admin/listauctions.php">{L_067}</a></li>
                        	<li><a href="{SITEURL}admin/listclosedauctions.php">{L_214}</a></li>
                        	<li><a href="{SITEURL}admin/listsuspendedauctions.php">{L_5227}</a></li>
                        </ul>
                    </div>
                </div>
            	<div class="box">
                	<h4 class="rounded-top">Tips and Info</h4>
                    <div class="rounded-bottom">
                    	You can manage open or live auctions, old closed auctions or suspend a particular auction. You can also edit an auction if you wish to for a seller. This feature is useful if a seller lists lot of external links in the descriptions.</div>
                </div>