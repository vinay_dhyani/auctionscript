            	<div class="box">
                	<h4 class="rounded-top">{L_25_0009}</h4>
                    <div class="rounded-bottom">
                    	<ul class="menu">
                        	<li><a href="{SITEURL}admin/theme.php">{L_26_0002}</a></li>
                        	<li><a href="{SITEURL}admin/clearcache.php">{L_30_0031}</a></li>
                        </ul>
                    </div>
                </div>
            	<div class="box">
                	<h4 class="rounded-top">Tips and Info</h4>
                    <div class="rounded-bottom">
                    	Here You can edit theme files, select a particular theme, etc. You can duplicate theme folder and rename it and modify the tpl files to create a new theme. If you would like to have a custom designed theme - please contact us at zeescripts.com with your design details to obtain quote.
                    </div>
                </div>