            	<div class="box">
                	<h4 class="rounded-top">{L_25_0023}</h4>
                    <div class="rounded-bottom">
                    	<ul class="menu">
                        	<li><a href="{SITEURL}admin/stats_settings.php">{L_5142}</a></li>
                        	<li><a href="{SITEURL}admin/viewaccessstats.php">{L_5143}</a></li>
                        	<li><a href="{SITEURL}admin/viewbrowserstats.php">{L_5165}</a></li>
                        	<li><a href="{SITEURL}admin/viewplatformstats.php">{L_5318}</a></li>
                        </ul>
                    </div>
                </div>
            	<div class="box">
                	<h4 class="rounded-top">Tips and Info</h4>
                    <div class="rounded-bottom">
                    	Site statistics are displayed here like page views, unique number of visitors to your website, number of user sessions, browser statistics, platform statistics and so on. You can also enable or disable Statistics settings for access to reduce load on your server by clicking on Statistics - Site Settings
               	    . </div>
                </div>