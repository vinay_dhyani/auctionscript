            	<div class="box">
                	<h4 class="rounded-top">{L_25_0012}</h4>
                    <div class="rounded-bottom">
                    	<ul class="menu">
                        	<li><a href="{SITEURL}admin/fees.php">{L_25_0012}</a></li>
                        	<li><a href="{SITEURL}admin/fee_gateways.php">{L_445}</a></li>
                        	<li><a href="{SITEURL}admin/enablefees.php">{L_395}</a></li>
                        	<li><a href="{SITEURL}admin/accounts.php">{L_854}</a></li>
                        </ul>
                    </div>
                </div>
            	<div class="box">
                	<h4 class="rounded-top">Tips and info</h4>
                    <div class="rounded-bottom">
                    	Here you can manage FEES and Payment settings. You can enable or disable fee for free listings. You can also make the listings to Live Mode (pay and list) or Balance Mode (Credits for payment later). You can also set payment gateways whichever you wish to enable for sellers to accept payments from buyers like Paypal, Authorize.net, Worldpay, Moneybookers and 2checkout. </div>
                </div>