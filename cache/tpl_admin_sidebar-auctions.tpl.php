<div class="box">
                	<h4 class="rounded-top"><?php echo ((isset($this->_rootref['L_239'])) ? $this->_rootref['L_239'] : ((isset($MSG['239'])) ? $MSG['239'] : '{ L_239 }')); ?></h4>
                    <div class="rounded-bottom">
                    	<ul class="menu">
                        	<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>admin/listauctions.php"><?php echo ((isset($this->_rootref['L_067'])) ? $this->_rootref['L_067'] : ((isset($MSG['067'])) ? $MSG['067'] : '{ L_067 }')); ?></a></li>
                        	<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>admin/listclosedauctions.php"><?php echo ((isset($this->_rootref['L_214'])) ? $this->_rootref['L_214'] : ((isset($MSG['214'])) ? $MSG['214'] : '{ L_214 }')); ?></a></li>
                        	<li><a href="<?php echo (isset($this->_rootref['SITEURL'])) ? $this->_rootref['SITEURL'] : ''; ?>admin/listsuspendedauctions.php"><?php echo ((isset($this->_rootref['L_5227'])) ? $this->_rootref['L_5227'] : ((isset($MSG['5227'])) ? $MSG['5227'] : '{ L_5227 }')); ?></a></li>
                        </ul>
                    </div>
                </div>
            	<div class="box">
                	<h4 class="rounded-top">Tips and Info</h4>
                    <div class="rounded-bottom">
                    	You can manage open or live auctions, old closed auctions or suspend a particular auction. You can also edit an auction if you wish to for a seller. This feature is useful if a seller lists lot of external links in the descriptions.</div>
                </div>