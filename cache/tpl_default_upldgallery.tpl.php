<html>
<head>
<title><?php echo (isset($this->_rootref['SITENAME'])) ? $this->_rootref['SITENAME'] : ''; ?></title>
<link rel="stylesheet" type="text/css" href="themes/<?php echo (isset($this->_rootref['THEME'])) ? $this->_rootref['THEME'] : ''; ?>/style.css">
<style media="all" type="text/css">
.imgareaselect-border1 {
	background: url(images/border-v.gif) repeat-y left top;
}

.imgareaselect-border2 {
    background: url(images/border-h.gif) repeat-x left top;
}

.imgareaselect-border3 {
    background: url(images/border-v.gif) repeat-y right top;
}

.imgareaselect-border4 {
    background: url(images/border-h.gif) repeat-x left bottom;
}

.imgareaselect-border1, .imgareaselect-border2,
.imgareaselect-border3, .imgareaselect-border4 {
    opacity: 0.5;
    filter: alpha(opacity=50);
}

.imgareaselect-handle {
    background-color: #fff;
    border: solid 1px #000;
    opacity: 0.5;
    filter: alpha(opacity=50);
}

.imgareaselect-outer {
    background-color: #000;
    opacity: 0.5;
    filter: alpha(opacity=50);
}
</style>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript">
$(document).ready(function () {
	var num_images = $('#numimages', window.opener.document).val();
	var now_images = <?php echo (isset($this->_rootref['NUMIMAGES'])) ? $this->_rootref['NUMIMAGES'] : ''; ?>;
	var image_cost = <?php echo (isset($this->_rootref['IMAGE_COST'])) ? $this->_rootref['IMAGE_COST'] : ''; ?>;
	if (num_images != now_images) {
		var fee_diff = (now_images - num_images) * image_cost;
		var nowfee = $("#fee_exact", window.opener.document).val() + fee_diff;
		$("#fee_exact", window.opener.document).val(nowfee);
		$("#to_pay").text(Math.round(nowfee*1<?php echo (isset($this->_rootref['FEE_DECIMALS'])) ? $this->_rootref['FEE_DECIMALS'] : ''; ?>)/1<?php echo (isset($this->_rootref['FEE_DECIMALS'])) ? $this->_rootref['FEE_DECIMALS'] : ''; ?>);
		$('#numimages', window.opener.document).val(now_images);
	}
});
</script>
<?php if ($this->_rootref['B_CROPSCREEN']) {  ?>
<script type="text/javascript" src="js/jquery.imgareaselect.js"></script>
<script type="text/javascript">
function preview(img, selection) {
	var scaleX = <?php echo (isset($this->_rootref['SCALEX'])) ? $this->_rootref['SCALEX'] : ''; ?> / selection.width;
	var scaleY = <?php echo (isset($this->_rootref['SCALEY'])) ? $this->_rootref['SCALEY'] : ''; ?> / selection.height;

	$('#thumbprev').css({
		width: Math.round((scaleX / <?php echo (isset($this->_rootref['IMGRATIO'])) ? $this->_rootref['IMGRATIO'] : ''; ?>) * <?php echo (isset($this->_rootref['IMGWIDTH'])) ? $this->_rootref['IMGWIDTH'] : ''; ?>) + 'px',
		height: Math.round((scaleY / <?php echo (isset($this->_rootref['IMGRATIO'])) ? $this->_rootref['IMGRATIO'] : ''; ?>) * <?php echo (isset($this->_rootref['IMGHEIGHT'])) ? $this->_rootref['IMGHEIGHT'] : ''; ?>) + 'px',
		marginLeft: '-' + Math.round(scaleX * selection.x1) + 'px',
		marginTop: '-' + Math.round(scaleY * selection.y1) + 'px'
	});
	$('#x1').val(selection.x1 * <?php echo (isset($this->_rootref['IMGRATIO'])) ? $this->_rootref['IMGRATIO'] : ''; ?>);
	$('#y1').val(selection.y1 * <?php echo (isset($this->_rootref['IMGRATIO'])) ? $this->_rootref['IMGRATIO'] : ''; ?>);
	$('#x2').val(selection.x2 * <?php echo (isset($this->_rootref['IMGRATIO'])) ? $this->_rootref['IMGRATIO'] : ''; ?>);
	$('#y2').val(selection.y2 * <?php echo (isset($this->_rootref['IMGRATIO'])) ? $this->_rootref['IMGRATIO'] : ''; ?>);
	$('#w').val(selection.width * <?php echo (isset($this->_rootref['IMGRATIO'])) ? $this->_rootref['IMGRATIO'] : ''; ?>);
	$('#h').val(selection.height * <?php echo (isset($this->_rootref['IMGRATIO'])) ? $this->_rootref['IMGRATIO'] : ''; ?>);
}

$(document).ready(function () {
	$('#save_thumb').click(function() {
		var x1 = $('#x1').val();
		var y1 = $('#y1').val();
		var x2 = $('#x2').val();
		var y2 = $('#y2').val();
		var w = $('#w').val();
		var h = $('#h').val();
		if (x1=="" || y1=="" || x2=="" || y2=="" || w=="" || h=="") {
			alert("You must make a selection first");
			return false;
		} else {
			return true;
		}
	});
});

$(window).load(function () {
	$('#thumbnail').imgAreaSelect({ aspectRatio: '<?php echo (isset($this->_rootref['RATIO'])) ? $this->_rootref['RATIO'] : ''; ?>', onSelectChange: preview, x1: 0, y1: 0, x2: <?php echo (isset($this->_rootref['STARTX'])) ? $this->_rootref['STARTX'] : ''; ?>, y2: <?php echo (isset($this->_rootref['STARTY'])) ? $this->_rootref['STARTY'] : ''; ?> });
});

</script>
<?php } ?>
</head>

<body bgcolor="#FFFFFF">
<div class="container">
<?php if ($this->_rootref['B_CROPSCREEN']) {  ?>
<div style="color:#000000;" align="center">
	<p><?php echo ((isset($this->_rootref['L_610'])) ? $this->_rootref['L_610'] : ((isset($MSG['610'])) ? $MSG['610'] : '{ L_610 }')); ?></p>
	<img src="<?php echo (isset($this->_rootref['IMGPATH'])) ? $this->_rootref['IMGPATH'] : ''; ?>" style="<?php echo (isset($this->_rootref['SWIDTH'])) ? $this->_rootref['SWIDTH'] : ''; ?>" id="thumbnail" alt="Create Thumbnail">
	<p><?php echo ((isset($this->_rootref['L_613'])) ? $this->_rootref['L_613'] : ((isset($MSG['613'])) ? $MSG['613'] : '{ L_613 }')); ?></p>
	<div style="overflow:hidden; border:#000000 double; <?php echo (isset($this->_rootref['THUMBWH'])) ? $this->_rootref['THUMBWH'] : ''; ?>">
		<img src="<?php echo (isset($this->_rootref['IMGPATH'])) ? $this->_rootref['IMGPATH'] : ''; ?>" style="position: relative;" alt="Thumbnail Preview" id="thumbprev">
	</div>
	<form name="thumbnail" action="?action=crop&img=<?php echo (isset($this->_rootref['IMAGE'])) ? $this->_rootref['IMAGE'] : ''; ?>" method="post">
    	<input type="hidden" name="csrftoken" value="<?php echo (isset($this->_rootref['_CSRFTOKEN'])) ? $this->_rootref['_CSRFTOKEN'] : ''; ?>">
		<input type="hidden" name="x1" value="0" id="x1">
		<input type="hidden" name="y1" value="0" id="y1">
		<input type="hidden" name="x2" value="<?php echo (isset($this->_rootref['STARTX'])) ? $this->_rootref['STARTX'] : ''; ?>" id="x2">
		<input type="hidden" name="y2" value="<?php echo (isset($this->_rootref['STARTY'])) ? $this->_rootref['STARTY'] : ''; ?>" id="y2">
		<input type="hidden" name="w" value="50" id="w">
		<input type="hidden" name="h" value="50" id="h">
		<input type="submit" class="button" name="upload_thumbnail" value="<?php echo ((isset($this->_rootref['L_616'])) ? $this->_rootref['L_616'] : ((isset($MSG['616'])) ? $MSG['616'] : '{ L_616 }')); ?>" id="save_thumb"><input type="submit" class="button" name="upload_thumbnail" value="<?php echo ((isset($this->_rootref['L_618'])) ? $this->_rootref['L_618'] : ((isset($MSG['618'])) ? $MSG['618'] : '{ L_618 }')); ?>" >
	</form>
	<span class="smallspan"><?php echo ((isset($this->_rootref['L_629'])) ? $this->_rootref['L_629'] : ((isset($MSG['629'])) ? $MSG['629'] : '{ L_629 }')); ?></span>
</div>
<?php } else { ?>
<form name="upload" action="" method="post" enctype="multipart/form-data">
<input type="hidden" name="csrftoken" value="<?php echo (isset($this->_rootref['_CSRFTOKEN'])) ? $this->_rootref['_CSRFTOKEN'] : ''; ?>">
<table cellpadding="3" cellspacing="0" border="0" align="center" width="90%">
	<tr>
		<td bgcolor="<?php echo (isset($this->_rootref['HEADERCOLOUR'])) ? $this->_rootref['HEADERCOLOUR'] : ''; ?>" colspan="2">
			<b><?php echo ((isset($this->_rootref['L_663'])) ? $this->_rootref['L_663'] : ((isset($MSG['663'])) ? $MSG['663'] : '{ L_663 }')); ?></b>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<?php echo ((isset($this->_rootref['L_673'])) ? $this->_rootref['L_673'] : ((isset($MSG['673'])) ? $MSG['673'] : '{ L_673 }')); ?> <?php echo (isset($this->_rootref['MAXIMAGES'])) ? $this->_rootref['MAXIMAGES'] : ''; ?> <?php echo ((isset($this->_rootref['L_674'])) ? $this->_rootref['L_674'] : ((isset($MSG['674'])) ? $MSG['674'] : '{ L_674 }')); ?><br>
			<?php //echo ((isset($this->_rootref['L_679'])) ? $this->_rootref['L_679'] : ((isset($MSG['679'])) ? $MSG['679'] : '{ L_679 }')); ?>
		</td>
	</tr>
	<?php if ($this->_rootref['ERROR'] != ('')) {  ?>
	<tr>
		<td class="errfont" align="center" colspan="2">
			<?php echo (isset($this->_rootref['ERROR'])) ? $this->_rootref['ERROR'] : ''; ?>
		</td>
	</tr>
	<?php } if ($this->_rootref['B_CANUPLOAD']) {  ?>
	<tr>
	<style>
		input[type="file"] {
		display: none;
		
		}
		.custom-file-upload {
			background-image: url("/images/plus.gif");
			background-repeat: no-repeat;
			display: inline-block;
			
			cursor: pointer;
			
		}
	</style>
		<td width="30%">
		
		<?php //echo ((isset($this->_rootref['L_680'])) ? $this->_rootref['L_680'] : ((isset($MSG['680'])) ? $MSG['680'] : '{ L_680 }')); ?><br>
			<label class="custom-file-upload">
			<input type="file" name="userfile" size="15"> <span style="margin:20px;">Add Images</span>
			</label>
		</td>
		<td style="float:center;" width="70%">
			 <?php //echo ((isset($this->_rootref['L_681'])) ? $this->_rootref['L_681'] : ((isset($MSG['681'])) ? $MSG['681'] : '{ L_681 }')); ?><br>
			<input type="submit" name="uploadpicture" value="Upload Image..<?php //echo ((isset($this->_rootref['L_681'])) ? $this->_rootref['L_681'] : ((isset($MSG['681'])) ? $MSG['681'] : '{ L_681 }')); ?>">
		</td>
	</tr>
	<!--<tr>
		<td>
			2. <?php //echo ((isset($this->_rootref['L_681'])) ? $this->_rootref['L_681'] : ((isset($MSG['681'])) ? $MSG['681'] : '{ L_681 }')); ?><br>
			<input type="submit" name="uploadpicture" value="<?php //echo ((isset($this->_rootref['L_681'])) ? $this->_rootref['L_681'] : ((isset($MSG['681'])) ? $MSG['681'] : '{ L_681 }')); ?>">
		</td>
	</tr>-->
	<tr>
		<td colspan="2">
			<?php //echo ((isset($this->_rootref['L_682'])) ? $this->_rootref['L_682'] : ((isset($MSG['682'])) ? $MSG['682'] : '{ L_682 }')); ?>
		</td>
	</tr>
	<?php } else { ?>
	<tr>
		<td class="errfont" align="center" colspan="2">
			<?php echo ((isset($this->_rootref['L_688'])) ? $this->_rootref['L_688'] : ((isset($MSG['688'])) ? $MSG['688'] : '{ L_688 }')); ?> <?php echo (isset($this->_rootref['MAXIMAGES'])) ? $this->_rootref['MAXIMAGES'] : ''; ?> <?php echo ((isset($this->_rootref['L_689'])) ? $this->_rootref['L_689'] : ((isset($MSG['689'])) ? $MSG['689'] : '{ L_689 }')); ?>
		</td>
	</tr>
	<?php } ?>
</table>
<?php } ?>
<br style="clear:both;">
<br>
<center>
<b><?php echo ((isset($this->_rootref['L_687'])) ? $this->_rootref['L_687'] : ((isset($MSG['687'])) ? $MSG['687'] : '{ L_687 }')); ?></b>
</center>
<table cellpadding="3" cellspacing="0" border="0" align="center" width="90%">
	<tr bgcolor="<?php echo (isset($this->_rootref['HEADERCOLOUR'])) ? $this->_rootref['HEADERCOLOUR'] : ''; ?>">
		<td width="46%">
			<b><?php echo ((isset($this->_rootref['L_684'])) ? $this->_rootref['L_684'] : ((isset($MSG['684'])) ? $MSG['684'] : '{ L_684 }')); ?></b>
		</td>
		<td width="30%">
			<b><?php echo ((isset($this->_rootref['L_685'])) ? $this->_rootref['L_685'] : ((isset($MSG['685'])) ? $MSG['685'] : '{ L_685 }')); ?></b>
		</td>
		<td width="12%" align="center">
			<b><?php echo ((isset($this->_rootref['L_008'])) ? $this->_rootref['L_008'] : ((isset($MSG['008'])) ? $MSG['008'] : '{ L_008 }')); ?></b>
		</td>
		<td width="12%" align="center">
			<b><?php echo ((isset($this->_rootref['L_686'])) ? $this->_rootref['L_686'] : ((isset($MSG['686'])) ? $MSG['686'] : '{ L_686 }')); ?></b>
		</td>
	</tr>
	<?php $_images_count = (isset($this->_tpldata['images'])) ? sizeof($this->_tpldata['images']) : 0;if ($_images_count) {for ($_images_i = 0; $_images_i < $_images_count; ++$_images_i){$_images_val = &$this->_tpldata['images'][$_images_i]; ?>
	<tr>
		<td>
			<?php echo $_images_val['IMGNAME']; ?>
		</td>
		<td>
			<?php echo $_images_val['IMGSIZE']; ?>
		</td>
		<td align="center">
			<a href="?action=delete&img=<?php echo $_images_val['ID']; ?>"><IMG SRC="images/trash.gif" border="0"></a>
		</td>
		<td align="center">
			<a href="?action=makedefault&img=<?php echo $_images_val['IMGNAME']; ?>"><img src="images/<?php echo $_images_val['DEFAULT']; ?>" border="0"></a>
		</td>
	</tr>
	<?php }} ?>
</table>
<br><br>
<center>
	<input type="submit" name="creategallery" value="<?php echo ((isset($this->_rootref['L_683'])) ? $this->_rootref['L_683'] : ((isset($MSG['683'])) ? $MSG['683'] : '{ L_683 }')); ?>">
</center>
</form>
<br><br>
<center>
	<a href="javascript: window.close()"><?php echo 'Cancel'//echo ((isset($this->_rootref['L_678'])) ? $this->_rootref['L_678'] : ((isset($MSG['678'])) ? $MSG['678'] : '{ L_678 }')); ?></a>
</center>
</div>
</body>
</html>;