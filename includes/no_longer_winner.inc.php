<?php
/***************************************************************************
 *   ZeeAuctions Ebay Clone
 *   site					: http://www.zeeauctions.com/
 ***************************************************************************/

/***************************************************************************
 *   This program is distributed under the GNU General Public License v2.
 ***************************************************************************/

if (!defined('InEbayClone')) exit();

$item_title = $system->uncleanvars($item_title);

$emailer = new email_class();
$emailer->assign_vars(array(
		'SITE_URL' => $system->SETTINGS['siteurl'],
		'SITENAME' => $system->SETTINGS['sitename'],

		'C_NAME' => $OldWinner_name,
		'C_BID' => $OldWinner_bid,

		'N_BID' => $new_bid,

		'A_TITLE' => $item_title,
		'A_ENDS' => $ends_string,
		'A_PICURL' => ($pict_url_plain != '') ? $uploaded_path . $item_id . '/' . $pict_url_plain : 'images/email_alerts/default_item_img.jpg',
		'A_URL' => $system->SETTINGS['siteurl'] . 'item.php?id=' . $item_id
		));
$emailer->email_uid = $OldWinner_id;
$emailer->email_sender($OldWinner_email, 'no_longer_winner.inc.php', $system->SETTINGS['sitename'] . ' ' . $MSG['906'] . ': ' . $item_title);
?>