<?php
/***************************************************************************
 *   ZeeAuctions Ebay Clone
 *   site					: http://www.zeeauctions.com/
 ***************************************************************************/

/***************************************************************************
 *   This program is distributed under the GNU General Public License v2.
 ***************************************************************************/

if (!defined('InEbayClone')) exit();

// Check if the e-mail has to be sent or not
$query = "SELECT endemailmode FROM " . $DBPrefix . "users WHERE id = " . $Seller['id'];
$res = mysql_query($query);
$system->check_mysql($res, $query, __LINE__, __FILE__);
$emailmode = mysql_result($res, 0, 'endemailmode');

if ($emailmode == 'one') {
	$emailer = new email_class();
	$emailer->assign_vars(array(
			'S_NAME' => $Seller['name'],
			'S_NICK' => $Seller['nick'],
			'S_EMAIL' => $Seller['email'],
			'A_TITLE' => $Auction['title'],
			'A_ID' => $Auction['id'],
			'A_END' => $ends_string,
			'A_URL' => $system->SETTINGS['siteurl'] . 'item.php?id=' . $Auction['id'],
			'SITE_URL' => $system->SETTINGS['siteurl'],
			'A_PICURL' => ($pict_url != '') ? $system->SETTINGS['siteurl'] . $uploaded_path . $auction_id . '/' . $Auction['pict_url'] : $system->SETTINGS['siteurl'] . 'images/email_alerts/default_item_img.jpg',
			'SITENAME' => $system->SETTINGS['sitename']
			));
	$emailer->email_uid = $Seller['id'];
	$emailer->email_sender($Seller['email'], 'endauction_nowinner.inc.php', $system->SETTINGS['sitename'] . ' ' . $MSG['112']);
}
?>