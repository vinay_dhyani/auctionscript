<?php
/***************************************************************************
 *   ZeeAuctions Ebay Clone
 *   site					: http://www.zeeauctions.com/
 ***************************************************************************/

/***************************************************************************
 *   This program is distributed under the GNU General Public License v2.
 ***************************************************************************/

if (basename($_SERVER['PHP_SELF']) != 'user_login.php')
{
	// Check if we are in Maintainance mode
	// And if the logged in user is the superuser
	if ($system->check_maintainance_mode())
	{
		echo stripslashes($system->SETTINGS['MAINTAINANCE']['maintainancetext']);
		exit;
	}
}
?>