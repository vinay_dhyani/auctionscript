<?php
/***************************************************************************
 *   ZeeAuctions Ebay Clone
 *   site					: http://www.zeeauctions.com/
 ***************************************************************************/

/***************************************************************************
 *   This program is distributed under the GNU General Public License v2.
 ***************************************************************************/

if (!defined('InEbayClone')) exit();

$template->assign_vars(array(
	'B_ISERROR' => (!empty($ERR)),
	'B_MENUTITLE' => (!empty($TMP_usmenutitle)),
	'UCP_ERROR' => (isset($ERR)) ? $ERR : '',
	'UCP_TITLE' => (isset($TMP_usmenutitle)) ? $TMP_usmenutitle : ''
));
?>