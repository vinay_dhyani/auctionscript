<?php
/***************************************************************************
 *   ZeeAuctions Ebay Clone
 *   site					: http://www.zeeauctions.com/
 ***************************************************************************/

/***************************************************************************
 *   This program is distributed under the GNU General Public License v2.
 ***************************************************************************/

if (!defined('InEbayClone')) exit();

$emailer = new email_class();
$emailer->assign_vars(array(
		'SITE_URL' => $system->SETTINGS['siteurl'],
		'SITENAME' => $system->SETTINGS['sitename'],
		'ADMINMAIL' => $system->SETTINGS['adminmail'],

		'REPORT' => $report,

		'S_NAME' => $row['name']
		));
$emailer->email_uid = $row['id'];
$emailer->email_sender($row['email'], 'endauction_cumulative.inc.php', $MSG['25_0199']);
?>
