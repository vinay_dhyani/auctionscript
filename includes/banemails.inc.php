<?php
/***************************************************************************
 *   ZeeAuctions Ebay Clone
 *   site					: http://www.zeeauctions.com/
 ***************************************************************************/

/***************************************************************************
 *   This program is distributed under the GNU General Public License v2.
 ***************************************************************************/

// Retrieve banned free mail domains
if (!defined('InEbayClone')) exit();

$DOMAINS = $system->SETTINGS['banemail'];

$BANNEDDOMAINS = array_filter(explode("\n", $DOMAINS), 'chop');
if (count($BANNEDDOMAINS) > 0)
{
	$TPL_domains_alert = $MSG['30_0053'] . '<ul>';
	foreach ($BANNEDDOMAINS as $k => $v)
	{
		$TPL_domains_alert .= '<li><b>' . $v . '</b></li>';
	}
	$TPL_domains_alert .= '</ul>';
}
else
{
	$TPL_domains_alert = '';
}

function BannedEmail($email, $domains)
{
	$dom = explode('@', $email);
	$domains = array_map('chop', $domains);
	if (in_array($dom[1],$domains))
	{
		return true;
	}
	else
	{
		return false;
	}
}
?>