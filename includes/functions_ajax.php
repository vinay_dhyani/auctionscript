<?php
/***************************************************************************
 *   ZeeAuctions Ebay Clone
 *   site					: http://www.zeeauctions.com/
 ***************************************************************************/

/***************************************************************************
 *   This program is distributed under the GNU General Public License v2.
 ***************************************************************************/

if (!defined('InEbayClone')) exit('Access denied');

// author: John	http://www.zeeauctions.com/forums/member.php?5491-John
function converter_call($post_data = true, $data = array())
{
	global $system;
	include $include_path . 'converter.inc.php';

	// get convertion data
	if ($post_data)
	{
		global $_POST;
		$amount = $_POST['amount'];
		$from = $_POST['from'];
		$to = $_POST['to'];
	}
	else
	{
		$amount = $data['amount'];
		$from = $data['from'];
		$to = $data['to'];
	}
	$amount = $system->input_money($amount);

	$CURRENCIES = CurrenciesList();

	$conversion = ConvertCurrency($from, $to, $amount);
	// construct string
	echo $amount . ' ' . $CURRENCIES[$from] . ' = ' . $system->print_money($conversion, true, false, false) . ' ' . $CURRENCIES[$to];
}
?>