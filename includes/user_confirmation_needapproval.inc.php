<?php
/***************************************************************************
 *   ZeeAuctions Ebay Clone
 *   site					: http://www.zeeauctions.com/
 ***************************************************************************/

/***************************************************************************
 *   This program is distributed under the GNU General Public License v2.
 ***************************************************************************/

if (!defined('InEbayClone')) exit();

$emailer = new email_class();
$emailer->assign_vars(array(
		'C_ID' => addslashes($TPL_id_hidden),
		'C_NAME' => addslashes($TPL_name_hidden),
		'C_NICK' => addslashes($TPL_nick_hidden),
		'C_ADDRESS' => addslashes($_POST['TPL_address']),
		'C_CITY' => addslashes($_POST['TPL_city']),
		'C_PROV' => addslashes($_POST['TPL_prov']),
		'C_ZIP' => addslashes($_POST['TPL_zip']),
		'C_COUNTRY' => addslashes($_POST['TPL_country']),
		'C_PHONE' => addslashes($_POST['TPL_phone']),
		'C_EMAIL' => addslashes($_POST['TPL_email']),
		'C_PASSWORD' => addslashes($TPL_password_hidden),
		'SITENAME' => $system->SETTINGS['sitename'],
		'SITEURL' => $system->SETTINGS['siteurl'],
		'ADMINEMAIL' => $system->SETTINGS['adminmail'],
		'CONFIRMATION_PAGE' => $system->SETTINGS['siteurl'] . 'confirm.php?id=' . $TPL_id_hidden . '&hash=' . md5($MD5_PREFIX . $TPL_nick_hidden),
		'LOGO' => $system->SETTINGS['siteurl'] . 'themes/' . $system->SETTINGS['theme'] . '/' . $system->SETTINGS['logo']
		));
$emailer->email_uid = $TPL_id_hidden;
$emailer->email_sender(array($TPL_email_hidden, $system->SETTINGS['adminmail']), 'user_needapproval.inc.php', $system->SETTINGS['sitename']. ' '.$MSG['098']);
?>