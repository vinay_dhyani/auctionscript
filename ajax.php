<?php
/***************************************************************************
 *   ZeeAuctions Ebay Clone
 *   site					: http://www.zeeauctions.com/
 ***************************************************************************/

/***************************************************************************
 *   This program is distributed under the GNU General Public License v2.
 ***************************************************************************/

include 'includes/common.inc.php';
include $include_path . 'functions_ajax.php';

switch ($_GET['do'])
{
	case 'converter':
		converter_call();
		break;
}
?>
